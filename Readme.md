# patch-assessment

> Your task is to build a simple API to manage a library’s book inventory.
> There are two main components of this API: management endpoints (to be used
> by the librarians) and user-facing endpoints for end-user actions.
>
> Books are referenced by their ISBN. The library can have more than one copy
> of any given book (multiple copies of the same ISBN).
>
> API endpoints to be built:
>
> Librarian Endpoints:
>
> * An endpoint to add a book (by ISBN) to the library.
> * An endpoint to remove a book (by its internal Id) from the library.
> * An endpoint that generates a list of all overdue books.
>
> User Endpoints:
>
> * An endpoint to check out a book (assume a two-week checkout period from time
>   of call). A User can check out any book except when:
>   - They currently have 3 checked out books.
>   - They are overdue on returning any book.
> * An endpoint to return a checked-out book.
> * An endpoint that returns all currently checked out books for that user.
>
> For the purposes of this exercise, we can assume there is a librarian user 
> (userId 1) and three regular users (userIds 2, 3, 4). You can hardcode this 
> table. Also, no need to worry about authentication, etc.

## Build

[Docker](https://docker.com/) is used to provision a local \*AMP stack and
[Make](https://www.gnu.org/software/make/) is used to perform build actions.
Be sure these programs are installed before proceeding.

To build the project, first navigate to the root of the project directory
and copy `env.example` to `.env`. This file contains configuration values
relevant to the development environment itself (eg. project ID, PHP version,
etc).

    cd patch-assessment
    cp env.example .env

Next, execute `make` to launch the build process. The build output is placed
in `./dist`.

    make
    ls ./dist/*

To serve the API after building it, execute `make start` to launch the
containerized services. (The containers make take a few moments to fully
start, specifically MySQL.)

    make start

Rename the `env.example` found in the output directory. This file contains
configuration values relevant to running the API, for example MySQL connection
credentials.

    cp ./dist/env.example ./dist/.env

\*Be sure not to confuse the build configuration `.env` in the project’s root
directory with the `.env` file in the `dist` directory. The root directory’s
file contains configuration values used by the Docker suite to provision
the development environment. The `dist` directory’s file contains configuration
values used by the website itself.

Finally, execute `make init-db` to initialize the database.

    make init-db

The API is now available locally at [http://localhost:8080](http://localhost:8080).

## Tests

To run basic integration tests, execute `make test`. However, there are two
points worth mentioning before running tests. 1) Tests rely on `curl` being
installed locally. 2) Integration tests initiated by `make` will automatically
trigger a re-initialization of the database.

    make test

You may execute the `tests.sh` script manually to run the tests without
re-initializing the database, but some tests may unexpectedly fail.

## Cleanup

To clean-up and delete the project, first execute `make stop` to halt the
containerized services.

    make stop

Then, execute `make purge` to delete the build artifacts and remove the volume
store used by the MySQL Docker image.

    make purge

You may now safely delete the project directory.

## Examples

Add a new book to the library:

    curl -X POST \
      -H "Authorization: Bearer 1" \
      -H "Content-type: application/json" \
      -d '{"isbn": "123456789123X", "title": "How to Foo Bar",
          "author_last_name": "Bull", "author_first_name": "Tara", "qty": 3}' \
      http://localhost:8080/api/books

Delete a book from the library:

    curl -X DELETE \
      -H "Authorization: Bearer 1" \
      http://localhost:8080/api/books/11

List books:

    # librarian listing all borrowed books
    curl -X GET \
      -H "Authorization: Bearer 1" \
      http://localhost:8080/api/books

    # librarian listing borrowed books account
    curl -X GET \
      -H "Authorization: Bearer 1" \
      http://localhost:8080/api/books?account_id=2

    # librarian listing only overdue books
    curl -X GET \
      -H "Authorization: Bearer 1" \
      http://localhost:8080/api/books?overdue=true

    # librarian listing overdue books for account
    curl -X GET \
      -H "Authorization: Bearer 1" \
      http://localhost:8080/api/books?account_id=2&overdue=true

    # patron listing borrowed books
    curl -X GET \
      -H "Authorization: Bearer 2" \
      http://localhost:8080/api/books

    # patron listing overdue books
    curl -X GET \
      -H "Authorization: Bearer 2" \
      http://localhost:8080/api/books?overdue=true

Borrow a book from the library:

    curl -X POST \
      -H "Authorization: Bearer 2" \
      http://localhost:8080/api/books/borrow/9780743273565

Return a book to the library:

    curl -X POST \
      -H "Authorization: Bearer 2" \
      http://localhost:8080/api/books/return/9780743273565

Return all books to the library:

    curl -X POST \
      -H "Authorization: Bearer 2" \
      http://localhost:8080/api/books/return/all
