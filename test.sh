#!/bin/sh

set -o allexport
source ./.env
set +o allexport

CURL="curl --silent --write-out HTTPSTATUS:%{http_code}"

executeTest()
{
    echo "$1"

    http_status=$(echo "$2" | tr -d '\n' | sed -e 's/.*HTTPSTATUS://')
    http_body=$(echo "$2" | sed -e 's/HTTPSTATUS\:.*//g')
    
    if [ "$http_status" != "$3" ]; then
        echo HTTP/$http_status
        echo $http_body
        exit
    fi
    [ $# == 4 ] && echo $http_body
    echo "OK"
}

executeTest "Test add book" \
    "$($CURL -X POST \
        -H "Authorization: Bearer 1" \
        -H "Content-type: application/json" \
        -d '{"isbn": "123456789123X", "title": "How to Foo Bar",
            "author_last_name": "Bull", "author_first_name": "Tara", "qty": 3}' \
        http://localhost:8080/api/books)" \
    201

executeTest "Test delete book" \
    "$($CURL -X DELETE \
        -H "Authorization: Bearer 1" \
        http://localhost:8080/api/books/11)" \
    200

executeTest "Test borrow books" \
    "$($CURL -X POST \
        -H "Authorization: Bearer 2" \
        http://localhost:8080/api/books/borrow/9780743273565)" \
    200

# borrow 2 more books
curl --silent -X POST \
    -H "Authorization: Bearer 2" \
    http://localhost:8080/api/books/borrow/9780679722762 2&> /dev/null
curl --silent -X POST \
    -H "Authorization: Bearer 2" \
    http://localhost:8080/api/books/borrow/0446365386 2&> /dev/null

executeTest "Test borrow books - (denied, 3 books)" \
    "$($CURL -X POST \
        -H "Authorization: Bearer 2" \
        http://localhost:8080/api/books/borrow/0684833395)" \
    403

executeTest "Test return book" \
    "$($CURL -X POST \
        -H "Authorization: Bearer 2" \
        http://localhost:8080/api/books/return/0446365386)" \
    200

## backdate loan_date for overdue test
$(docker-compose exec mysql mysql -u root -p$MYSQL_ROOT_PASSWORD -e \
    "UPDATE ${COMPOSE_PROJECT_NAME}.loans SET loan_date = '2020-01-01' LIMIT 1") 2&> /dev/null

executeTest "Test borrow books - (denied, overdue)" \
    "$($CURL -X POST \
        -H "Authorization: Bearer 2" \
        http://localhost:8080/api/books/borrow/0684833395)" \
    403

executeTest "Test list borrowed books" \
    "$($CURL -X GET \
        -H "Authorization: Bearer 1" \
        http://localhost:8080/api/books)" \
    200 true

executeTest "Test list overdue books" \
    "$($CURL -X GET \
        -H "Authorization: Bearer 1" \
        http://localhost:8080/api/books?overdue=true)" \
    200 true

executeTest "Test return all books" \
    "$($CURL -X POST \
        -H "Authorization: Bearer 2" \
        http://localhost:8080/api/books/return/all)" \
    200

executeTest "Test confirm books are returned" \
    "$($CURL -X GET \
      -H "Authorization: Bearer 1" \
      http://localhost:8080/api/books?account_id=2)" \
    200 true
