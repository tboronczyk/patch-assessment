include .env

DOCKER_RUN=docker run --rm -v "$(PWD)":/app -w /app
PHP_IMAGE_ID=$(shell docker images -q -f reference=$(COMPOSE_PROJECT_NAME)_php)
MYSQL_CONTAINER_ID=$(shell docker ps -q -f name=$(COMPOSE_PROJECT_NAME)_mysql)


all: build

##
## Docker helpers
##

# `make start` to start all containers, or `make start c=name` to start a
# specific container.
start:
	docker-compose up -d $(c)
 
# stop containers
stop:
	docker-compose stop $(c)

# show status of all containers
status:
	docker-compose ps


##
## Build targets
##

# build the project
build:
	mkdir -p dist
	cp -R src/{db,include,public,routes} dist/
	cp src/{composer.json,composer.lock,dependencies.php,env.example} dist/

# Docker pulls images that are not already available, but since we rely on an
# ad-hoc build of PHP for Composer, we must check whether it's available
# ourselves.
ifeq ($(PHP_IMAGE_ID), )
	docker-compose build php
endif
	docker run --rm -v "$(PWD)/dist":/app -w /app $(COMPOSE_PROJECT_NAME)_php composer install

# initialize the containerized database
init-db:
ifeq ($(MYSQL_CONTAINER_ID), )
	$(error MySQL is not running)
endif
	docker-compose exec mysql mysql -u root -p$(MYSQL_ROOT_PASSWORD) -e \
	  "CREATE DATABASE IF NOT EXISTS $(COMPOSE_PROJECT_NAME); \
	  CREATE USER IF NOT EXISTS '$(COMPOSE_PROJECT_NAME)'@'%' IDENTIFIED BY 'password'; \
	  REVOKE ALL ON $(COMPOSE_PROJECT_NAME).* FROM '$(COMPOSE_PROJECT_NAME)'@'%'; \
	  GRANT SELECT, INSERT, UPDATE, DELETE ON $(COMPOSE_PROJECT_NAME).* TO '$(COMPOSE_PROJECT_NAME)'@'%'"
	docker-compose exec mysql sh -c \
	  "mysql -u root -p$(MYSQL_ROOT_PASSWORD) $(COMPOSE_PROJECT_NAME) < /app/db/schema.sql"

test: init-db
	sh ./test.sh

##
## Cleanup
##

# remove build artifacts
clean:
	rm -Rf dist

# remove build artifacts, build environment, containers, and volumes
# (ie: everything)
purge:
	docker-compose down -v
	rm -Rf dist


.PHONY: start stop status build init-db test clean purge
