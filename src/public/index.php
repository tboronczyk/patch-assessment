<?php
declare(strict_types=1);

chdir(dirname(__DIR__, 1));
require_once 'vendor/autoload.php';

Dotenv\Dotenv::createImmutable('.')->load();

(require_once 'dependencies.php')->get('App')->run();
