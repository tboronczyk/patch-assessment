<?php
declare(strict_types=1);

use Slim\Routing\RouteCollectorProxy;

/** @var $app Slim\App */

$app->group('/api', function (RouteCollectorProxy $group) {

    $group->group('/books', function (RouteCollectorProxy $group) {
        $group->get('', 'LibraryController:listBooks');
        $group->post('', 'BookController:addBook');
        $group->delete('/{id:\d+}', 'BookController:deleteBook');
        $group->post('/borrow/{isbn:.+}', 'LibraryController:borrowBook');
        $group->post('/return/{isbn:.+}', 'LibraryController:returnBook');
    });

})->add($app->getContainer()->get('AuthorizationMiddleware'));
