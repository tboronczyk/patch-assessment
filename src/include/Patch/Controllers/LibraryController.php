<?php
declare(strict_types=1);

namespace Patch\Controllers;

use Psr\Container\ContainerInterface as Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class LibraryController
 * @package Patch\Controllers
 */
class LibraryController extends Controller
{
    protected $container;

    public function __construct(Container $c)
    {
        $this->container = $c;
    }

    /**
     * Borrow a book from the library.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function borrowBook(Request $req, Response $resp, array $args): Response
    {
        $isbn = $args['isbn'];
        $account = $req->getAttribute('account');
        $accounts = $this->container->get('Accounts');
        $books = $this->container->get('Books');
        $loans = $this->container->get('Loans');

        $book = $books->getByIsbn($isbn);
        if (!$book || $book['qty'] < 1) {
            return $this->notFoundResponse($resp, 'Book not available');
        }

        $status = $accounts->status($account['id']);
        if ($status['bookCount'] >= 3 || $status['overdueCount'] > 0) {
            return $this->forbiddenResponse($resp, 'Not allowed to borrow');
        }

        $loans->loan($account['id'], $book['id']);

        return $resp;
    }

    /**
     * Return books to the library.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function returnBook(Request $req, Response $resp, array $args): Response
    {
        $isbn = $args['isbn'];
        $account = $req->getAttribute('account');
        $books = $this->container->get('Books');
        $loans = $this->container->get('Loans');

        // return all books
        if (strcasecmp($isbn, 'all') === 0) {
            $loans->receiveAll($account['id']);     
            return $resp;
        }

        // return a single book
        $book = $books->getByIsbn($isbn);
        if (!$book) {
            return $this->notFoundResponse($resp, 'Book not found');
        }

        $loans->receive($account['id'], $book['id']);

        return $resp;
    }

    /**
     * View books.
     *
     * For patron accounts, this returns a list of books that are on loan under
     * their account (ie an account filter is automatically applied).
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function listBooks(Request $req, Response $resp, array $args): Response
    {
        $account = $req->getAttribute('account');
        $loans = $this->container->get('Loans');

        $filters = [];
        if ($account['role'] == 'patron') {
            $filters['account_id'] = $account['id'];
        }

        $params = $req->getQueryParams();
        if ($account['role'] == 'staff') {
            if (isset($params['account_id'])) {
                $filters['account_id'] = $params['account_id'];
            }
        }
        if (isset($params['overdue'])) {
            $filters['loan_date'] = (new \DateTime())
                ->sub(new \DateInterval('P2W'))->format('Y-m-d');
        }

        return $resp->withJson($loans->listLoaned($filters));
    }
}
