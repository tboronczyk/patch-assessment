<?php
declare(strict_types=1);

namespace Patch\Controllers;

use Psr\Container\ContainerInterface as Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class BookController
 * @package Patch\Controllers
 */
class BookController extends Controller
{
    protected $container;

    public function __construct(Container $c)
    {
        $this->container = $c;
    }

    /**
     * Handle adding a book to the library by ISBN.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function addBook(Request $req, Response $resp, array $args): Response
    {
        if ($req->getAttribute('account')['role'] != 'staff') {
            return $this->unauthorizedresponse($resp, 'Insufficient privileges');
        }

        $data = $req->getParsedBody();
        $books = $this->container->get('Books');

        $isValid = $books->hasRequiredFields($data);
        if (!$isValid) {
            return $this->badRequestResponse($resp, 'Missing required fields');
        }

        $book = $books->getByIsbn($data['isbn']);
        if ($book) {
            return $this->badRequestResponse($resp, 'Book already exists');
        }

        $id = $books->create($data);
        return $resp->withJson(['id' => $id], 201);
    }

    /**
     * Handle removing a book from the library by record ID.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function deleteBook(Request $req, Response $resp, array $args): Response
    {
        $id = (int)$args['id'];
        $books = $this->container->get('Books');

        $book = $books->getById($id);
        if (!$book) {
            return $this->notFoundResponse($resp, 'Book not found');
        }

        if ($req->getAttribute('account')['role'] != 'staff') {
            return $this->unauthorizedresponse($resp, 'Insufficient privileges');
        }

        $books->delete($id);

        return $resp;
    }
}
