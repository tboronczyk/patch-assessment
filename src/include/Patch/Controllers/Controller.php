<?php
declare(strict_types=1);

namespace Patch\Controllers;

use Psr\Container\ContainerInterface as Container;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Class Controller
 * @package Patch\Controllers
 */
class Controller
{
    /**
     * Return a bad request response.
     *
     * @param Response $resp
     * @param ?string $msg
     * @return Response
     */
    protected function badRequestResponse(Response $resp, ?string $msg = null): Response
    {
        return $resp->withJson(['error' => $msg ?? 'bad request'], 400);
    }

    /**
     * Return an unauthorized response.
     *
     * @param Response $resp
     * @param ?string $msg
     * @return Response
     */
    protected function unauthorizedResponse(Response $resp, ?string $msg = null): Response
    {
        return $resp->withAddedHeader('WWW-Authenticate', 'Bearer')
            ->withJson(['error' => $msg ?? 'unauthorized'], 401);
    }

    /**
     * Return a forbidden response.
     *
     * @param Response $resp
     * @param ?string $msg
     * @return Response
     */
    protected function forbiddenResponse(Response $resp, ?string $msg = null): Response
    {
        return $resp->withJson(['error' => $msg ?? 'forbidden'], 403);
    }

    /**
     * Return a not-found response.
     *
     * @param Response $resp
     * @param ?string $msg
     * @return Response
     */
    protected function notFoundResponse(Response $resp, ?string $msg = null): Response
    {
        return $resp->withJson(['error' => $msg ?? 'not found'], 404);
    }
}
