<?php
declare(strict_types=1);

namespace Patch\Middleware;

use Psr\Container\ContainerInterface as Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;

/**
 * Class AuthorizationMiddleware
 * @package Patch\Middleware
 */
class AuthorizationMiddleware implements MiddlewareInterface
{
    protected $container;

    public function __construct(Container $c)
    {
        $this->container = $c;
    }

    /**
     * Accept JWT and block unauthenticated requests.
     *
     * @param Request $req
     * @param RequestHandler $handler
     * @return Response
     */
    public function process(Request $req, RequestHandler $handler): Response
    {
        try {
            $header = $req->getHeaderLine('Authorization');
            $accountId = trim((string)substr($header, strlen('Bearer ')));
            if (!ctype_digit($accountId)) {
                throw new \Exception('Invalid account ID format');
            }
            settype($accountId, 'int');

            $accounts = $this->container->get('Accounts');
            $account = $accounts->getById($accountId);
            if (!$account) {
                throw new \Exception('Invalid account');
            }
            $req = $req->withAttribute('account', $account);
        }
        catch (\Exception $e) {
            $resp = $this->container->get('ResponseFactory')->createResponse()
                ->withHeader('WWW-Authenticate', 'Bearer')
                ->withJson(['error' => 'Expired session'], 401);

            return $resp;
        }

        return $handler->handle($req);
    }
}
