<?php
declare(strict_types=1);

namespace Patch\Models;

use Boronczyk\Alistair\DbAccess;
use Psr\Container\ContainerInterface as Container;

/**
 * Class Loans
 * @package Patch\Models
 */
class Loans extends DbAccess
{
    protected $container;

    public function __construct(Container $c)
    {
        parent::__construct($c->get('db'));

        $this->container = $c;
    }

    /**
     * Ensure all fields are returned as the correct type.
     *
     * @param array $row
     * @return array
     */
    public function castFields(array $row): array
    {
        settype($row['book_id'], 'int');
        settype($row['account_id'], 'int');
        return $row;
    }

    /**
     * Loan a book out from the library.
     *
     * @param int $accountId
     * @param int $bookId
     */
    public function loan(int $accountId, int $bookId)
    {
        $this->query(
            'INSERT INTO loans (account_id, book_id, loan_date)
             VALUES (?, ?, ?)',
            [$accountId, $bookId, date('Y-m-d')]
        );

        $this->query(
            'UPDATE books SET qty = qty - 1 WHERE id = ?',
            [$bookId]
        );
    }

    /**
     * Return a book back to the library.
     *
     * @param int $accountId
     * @param int $bookId
     */
    public function receive(int $accountId, int $bookId)
    {
        $this->query(
            'DELETE FROM loans WHERE account_id = ? AND book_id = ?
             ORDER BY loan_date ASC LIMIT 1',
            [$accountId, $bookId]
        );

        $this->query(
            'UPDATE books SET qty = qty + 1 WHERE id = ?',
            [$bookId]
        );
    }

    /**
     * Return all of an account's books back to the library.
     *
     * @param int $accountId
     */
    public function receiveAll(int $accountId)
    {
        $ids = array_map(
            function ($row) {
                return $row['book_id'];
            },
            $this->queryRows(
                'SELECT book_id FROM loans WHERE account_id = ?',
                [$accountId]
            )
        );
        if (count($ids) < 1) {
            return;
        }
 
        $query = 'UPDATE books SET qty = qty + 1 WHERE id IN 
            (' . join(',', $ids) . ')';

        $this->query(
            'DELETE FROM loans WHERE account_id = ?',
            [$accountId]
        );
    }

    /**
     * Return a list of loaned books. Results may be filtered using:
     *
     *   - account_id <account_id>  for books under a specfic account
     *   - loan_date 'YYYY-mm-dd'   for overdue books
     *
     * @param array|null $filters
     * @result array
     */
    public function listLoaned(?array $filters): array
    {
        $clauses = [];
        $clauseValues = [];
        foreach ($filters ?? [] as $key => $value) {
            switch ($key) {
                case 'account_id':
                    $clauses[] = 'l.account_id = ?';
                    $clauseValues[] = $value;
                    break;
                case 'loan_date':
                    $clauses[] = 'l.loan_date < ?';
                    $clauseValues[] = $value;
                    break;
            }
        }

        $query = 'SELECT
            l.book_id, b.isbn, b.title, b.author_last_name, b.author_last_name,
            l.account_id, l.loan_date
            FROM loans l JOIN books b ON b.id = l.book_id';
        if ($clauses) {
            $query .= ' WHERE ' . join(' AND ', $clauses);
        }
        $query .= ' ORDER BY l.loan_date';

        return array_map(
            [$this, 'castFields'],
            $this->queryRows($query, $clauseValues)
        );
    }
}
