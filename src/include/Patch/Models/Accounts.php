<?php
declare(strict_types=1);

namespace Patch\Models;

use Boronczyk\Alistair\DbAccess;
use Psr\Container\ContainerInterface as Container;

/**
 * Class Accounts
 * @package Patch\Models
 */
class Accounts extends DbAccess
{
    protected $container;

    public function __construct(Container $c)
    {
        parent::__construct($c->get('db'));

        $this->container = $c;
    }

    /**
     * Ensure all fields are returned as the correct type.
     *
     * @param array $row
     * @return array
     */
    public function castFields(array $row): array
    {
        settype($row['id'], 'int');
        return $row;
    }


    /**
     * Return an account from the database by record ID.
     *
     * @param int $id
     * @return array
     */
    public function getById(int $id): array
    {
        $row = $this->queryRow(
            'SELECT id, last_name, first_name, role FROM accounts WHERE id = ?',
            [$id]
        );

        return ($row) ? $this->castFields($row) : [];
    }

    /**
     * Returns an account status overview.
     *
     * @param int $id
     * @return array
     */
     public function status(int $id): array
     {
        $date = (new \DateTime())
            ->sub(new \DateInterval('P2W'))->format('Y-m-d');
        $rows = $this->queryRows(
            'SELECT book_id, loan_date < "' . $date . '" AS overdue
            FROM loans WHERE account_id = ?',
            [$id]
        );

        $status = [
            'bookCount' => count($rows),
            'overdueCount' => 0
        ];
        foreach ($rows as $row) {
            $status['overdueCount'] += (int)$row['overdue'];
        }

        return $status;
     }
}
