<?php
declare(strict_types=1);

namespace Patch\Models;

use Boronczyk\Alistair\DbAccess;
use Psr\Container\ContainerInterface as Container;

/**
 * Class Books
 * @package Patch\Models
 */
class Books extends DbAccess
{
    protected $container;

    public function __construct(Container $c)
    {
        parent::__construct($c->get('db'));

        $this->container = $c;
    }

    /**
     * Return whether the provided data contains the required
     * fields to create a record.
     *
     * @param array $data
     * @return bool
     */
    public function hasRequiredFields(array $data): bool
    {
        return
            isset($data['isbn']) &&
            isset($data['title']) &&
            isset($data['author_last_name']) &&
            isset($data['author_first_name']) &&
            isset($data['qty']);
    }

    /**
     * Ensure all fields are returned as the correct type.
     *
     * @param array $row
     * @return array
     */
    public function castFields(array $row): array
    {
        settype($row['id'], 'int');
        settype($row['qty'], 'int');

        return $row;
    }


    /**
     * Return a book from the database by record ID.
     *
     * @param int $id
     * @return array
     */
    public function getById(int $id): array
    {
        $row = $this->queryRow(
            'SELECT id, isbn, title, author_last_name, author_first_name, qty
                FROM books WHERE id = ?',
            [$id]
        );

        return ($row) ? $this->castFields($row) : [];
    }


    /**
     * Return a book from the database by ISBN.
     *
     * @param string $isbn
     * @return array
     */
    public function getByIsbn(string $isbn): array
    {
        $row = $this->queryRow(
            'SELECT id, isbn, title, author_last_name, author_first_name, qty
                FROM books WHERE isbn = ?',
            [$isbn]
        );

        return ($row) ? $this->castFields($row) : [];
    }

    /**
     * Create a new book record in the database.
     *
     * @param array $data
     * @return int
     */
    public function create(array $data): int
    {
        $this->query(
            'INSERT INTO books (id, isbn, title, author_last_name, author_first_name, qty)
                VALUES (NULL, ?, ?, ?, ?, ?)',
            [
                $data['isbn'],
                $data['title'],
                $data['author_last_name'],
                $data['author_first_name'],
                $data['qty']
            ]
        );

        return (int)$this->db->lastInsertId();
    }

    /**
     * Delete a book record from the database by record ID.
     *
     * @param int $id
     */
    public function delete(int $id)
    {
        $this->query('DELETE FROM books WHERE id = ? LIMIT 1', [$id]);
    }
}
