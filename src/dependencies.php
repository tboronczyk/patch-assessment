<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Psr\Container\ContainerInterface as Container;
use Psr\Http\Message\ResponseFactoryInterface as ResponseFactory;
use Slim\App;
use Slim\Factory\AppFactory;
use Slim\Http\Factory\DecoratedResponseFactory;
use Slim\Psr7\Factory\ResponseFactory as SlimResponseFactory;
use Slim\Psr7\Factory\StreamFactory;
use Patch\Controllers\BookController;
use Patch\Controllers\LibraryController;
use Patch\Middleware\AuthorizationMiddleware;
use Patch\Models\Accounts;
use Patch\Models\Books;
use Patch\Models\Loans;

// All object instantiation and initialization should happen in this file.

$builder = new ContainerBuilder;

$builder->addDefinitions([
    'App' => function (Container $c): App {
        AppFactory::setContainer($c);
        $app = AppFactory::create();
    
        $env = $c->get('env');
        $app->addBodyParsingMiddleware();
        $app->addRoutingMiddleware();
        $app->addErrorMiddleware($env['DEBUG'], true, true);
    
        require_once 'routes/api.php';
    
        return $app;
    },

    'db' => function (Container $c): PDO {
        $env = $c->get('env');
        $pdo = new PDO($env['DB_DSN'], $env['DB_USERNAME'], $env['DB_PASSWORD']);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    },

    'env' => function (Container $c): array {
        $env = $_ENV;

        // normalize to bool
        $env['DEBUG'] = !(bool)strcasecmp('true', $env['DEBUG'] ?? 'false');

        return $env;
    }, 

    'ResponseFactory' => function (Container $c): ResponseFactory {
        return new DecoratedResponseFactory(
            new SlimResponseFactory,
            new StreamFactory
        );
    },
    
    // Patch models

    'Accounts' => function (Container $c): Accounts {
        return new Accounts($c);
    },

    'Books' => function (Container $c): Books {
        return new Books($c);
    },

    'Loans' => function (Container $c): Loans {
        return new Loans($c);
    },

    // Patch middleware

    'AuthorizationMiddleware' => function (Container $c): AuthorizationMiddleware {
        return new AuthorizationMiddleware($c);
    },

    // Patch controllers

    'BookController' => function (Container $c): BookController {
        return new BookController($c);
    },

    'LibraryController' => function (Container $c): LibraryController {
        return new LibraryController($c);
    }
]);

return $builder->build();    
